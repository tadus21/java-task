import java.util.Arrays;
import java.util.stream.IntStream;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class SequenceFinder {

    private static final int BASE_OF_3 = 3;

    private static final int SUM = 100;

    private static final String SEQUENCE_PATTERN = "1%s2%s3%s4%s5%s6%s7%s8%s9";
    private static final String[] OPERANDS = {"", "+", "-"};

    private static final int POSSIBLE_POSITIONS = 8;
    private static final int POSSIBLE_SEQUENCES = (int) Math.pow(OPERANDS.length, POSSIBLE_POSITIONS);

    public static void main(String[] args) {
        new SequenceFinder().printSequences();
    }


    private void printSequences() {
        IntStream.rangeClosed(0, POSSIBLE_SEQUENCES)
                .mapToObj(this::toOperandArray)
                .map(this::formatEvaluation)
                .filter(this::evaluate)
                .forEach(System.out::println);
    }

    /**
     * Calculates operands that should be put between sequence pattern by converting iteration number to base 3
     * and adding 0 till length of array is size of @see{@link #POSSIBLE_POSITIONS}.
     *
     * @param iteration indicates which combination of operands should be created
     * @return integer array representing operand combination
     */
    private int[] toOperandArray(int iteration) {
        int defaultOperand = 0;
        String operandCombination = Integer.toString(iteration, BASE_OF_3);
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < POSSIBLE_POSITIONS - operandCombination.length(); i++) {
            sb.append(defaultOperand);
        }
        return Arrays
                .stream((sb.toString() + operandCombination)
                        .split(""))
                .mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Formats @see {@link #SEQUENCE_PATTERN} into evaluation with provided operands.
     *
     * @param values Array of integers representing operands
     * @return evaluation represented as a string
     */
    private String formatEvaluation(int[] values) {
        return String.format(SEQUENCE_PATTERN, OPERANDS[values[0]], OPERANDS[values[1]],
                OPERANDS[values[2]], OPERANDS[values[3]], OPERANDS[values[4]],
                OPERANDS[values[5]], OPERANDS[values[6]], OPERANDS[values[7]]);
    }

    /**
     * Uses JavaScripts method 'eval' to calculate if evaluation is equal to 100. Js
     * engine is used since Java does not contain alternative method to this.
     *
     * @param evaluation evaluation represented as a string
     * @return returns true if evaluation result is equal to 100
     */
    private boolean evaluate(String evaluation) {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
        try {
            int result = (int) engine.eval(evaluation);
            return result == SUM;
        } catch (ScriptException e) {
            System.out.println("Failed to execute js code");
            throw new RuntimeException(e);
        }
    }

}
